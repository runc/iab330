﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProFare.Services;
using ProFare.Views;

namespace ProFare
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            GoogleMapsApiService.Initialize(Constants.GoogleMapsApiKey);
            MainPage = new NavigationPage(new MapPage2());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
