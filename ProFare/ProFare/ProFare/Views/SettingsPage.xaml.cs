﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProFare.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsPage : ContentPage
    {
        public SettingsPage()
        {
            InitializeComponent();
        }

        public async void OnTappingSettingsToHome(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MapPage2() { BindingContext = this.BindingContext }, false);

        }
    }
}